# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


def has_quorum(attendees_list, members_list):
    # remember to assign and get the length .
    num_attendees = len(attendees_list)
    num_mem = len(members_list)
    if num_attendees / num_mem == 0.50:
        return True
    else:
        return False
